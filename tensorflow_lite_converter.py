"""
This module converts tensorflow models into c array to be deployed in microcontroller

Author: Vahap
Date: 13-12-2021
"""

import tensorflow as tf
import os
import numpy as np


def generate_representative_dataset(folder_name: str = "representative_data"):
    """
    This is a generator function that provides a set of input data
    that's large enough to represent typical values.
    https://www.tensorflow.org/lite/performance/post_training_integer_quant#convert_using_float_fallback_quantization

    Keyword arguments:
    folder_name -- directory name that represents input data
    """
    # Retrieve file names to loop
    file_names = os.listdir(folder_name)
    # Loop through each file
    for file_name in file_names:
        complete_path = os.path.join(folder_name, file_name)
        # Load numpy file to return as representative data
        input_value = np.load(complete_path)
        print(np.shape(input_value))
        print(np.shape([input_value]))
        yield [input_value]


def convert_tensorflow_model_into_lite(model_type: str = "Keras",
                                       path_to_model: str = None) -> None:
    """
    Convert a tensorflow SavedModel or Keras H5 model
    into tensorflow lite format.

    Keyword arguments:
    path_to_model -- path to the model directory or model file
    model_type -- model type to be converted
    """
    # Load the model to pass to converter
    model = tf.keras.models.load_model(path_to_model)
    # Load the model to convert
    if model_type == "Keras":
        converter = tf.lite.TFLiteConverter.from_keras_model(model)
    elif model_type == "SavedModel":
        converter = tf.lite.TFLiteConverter.from_saved_model(model)
    else:
        print("Invalid model type")
        return
    # Optimize model for size and latency reduction
    # converter.optimizations = [tf.lite.Optimize.DEFAULT]
    # Provide a representative dataset
    # to quantize the variable data (such as model input/output and intermediates between layers)
    # converter.representative_dataset = generate_representative_dataset
    # Ensure that if any ops can't be quantized, the converter throws an error
    # converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    # Set the input and output tensors to uint8
    # converter.inference_input_type = tf.int8
    # converter.inference_output_type = tf.int8
    # Convert the model into lite format
    lite_model = converter.convert()

    # Open a file to embed and save the lite model
    with open('lite_model.tflite', 'wb') as f:
        f.write(lite_model)

    print("Lite model is created successfully!")


def convert_lite_model_into_c_array(model_name: str = None) -> None:
    """
    Convert a tensorflow lite model into c array format.

    Keyword argument:
    path_to_model -- path to the lite model file
    """
    # Run a bash (xxd) command from linux os to convert tflite file into c array
    tflite_extension_removal = -7
    os.system(f"xxd -i {model_name} > {model_name[:tflite_extension_removal]}.cpp")

    print("C++ lite model is created successfully!")


# Call functions to perform conversion
convert_tensorflow_model_into_lite(model_type="Keras", path_to_model="model.h5")
convert_lite_model_into_c_array(model_name="lite_model.tflite")

