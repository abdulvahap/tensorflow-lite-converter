# Tensorflow Lite Converter

This module converts tensorflow models into c array to be deployed in microcontrollers.

The script requires import of linux 'os' and installation of 'tensorflow' within the Python environment.

This file contains the following functions:

```python
import tensorflow as tf
import os

# converts tensorflow model into tensorflow lite
convert_tensorflow_model_into_lite(model_type="Keras", path_to_model="model.h5")

# converts tensorflow lite model into c array
convert_lite_model_into_cpp(model_name="lite_model.tflite")
```